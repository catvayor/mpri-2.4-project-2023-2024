(** Infer contains the logic to generate an inference constraint from
    an untyped term, that will elaborate to an explicitly-typed term
    or fail with a type error. *)

(* You have to implement the [has_type] function below,
   which is the constraint-generation function. *)

module Make(T : Utils.Functor) = struct
  module Constraint = Constraint.Make(T)
  open Constraint
  module Untyped = Untyped.Make(T)

  (** The "environment" of the constraint generator maps each program
      variable to an inference variable representing its (monomorphic)
      type.

      For example, to infer the type of the term [lambda x. t], we
      will eventually call [has_type env t] with an environment
      mapping [x] to a local inference variable representing its type.
  *)
  module Env = Untyped.Var.Map
  type env = variable Env.t

  type err = eq_error =
    | Clash of STLC.ty Utils.clash
    | Cycle of Constraint.variable Utils.cycle

  type 'a constraint_ = ('a, err) Constraint.t

  let eq v1 v2 = Eq(v1, v2)
  let decode v = MapErr(Decode v, fun e -> Cycle e)

  let assume_pair = function
    | [v1; v2] -> (v1, v2)
    | other ->
      Printf.ksprintf failwith
        "Error: this implementation currently only supports pairs,
         not tuples of size %d."
        (List.length other)

  (** This is a helper function to implement constraint generation for
      the [Annot] construct.
     
      [bind ty k] takes a type [ty], and a constraint [k] parametrized
      over a constraint variable. It creates a constraint context that
      binds a new constraint variable [?w] that must be equal to [ty],
      and places [k ?w] within this context.
      
      For example, if [ty] is the type [?v1 -> (?v2 -> ?v3)] , then
      [bind ty k] could be the constraint
        [∃(?w1 = ?v2 -> ?v3). ∃(?w2 = ?v1 -> ?w1). k ?w2], or equivalently
        [∃?w3 ?w4. ?w3 = ?v1 -> ?w4 ∧ ?w4 = ?v2 -> ?v3 ∧ k ?w3].
  *)
  let rec bind (ty : STLC.ty) (k : Constraint.variable -> ('a, 'e) t) : ('a, 'e) t =
    match ty with
    | Constr (Structure.Var v) ->
        let w = Var.fresh (Structure.TyVar.name v)
        in Exist (w, Some (Structure.Var v), k w)
    | Constr (Structure.Arrow (t1, t2)) ->
        let warr = Var.fresh "warr" in
        bind t1 (fun x -> bind t2 (fun wt -> Exist (warr, Some (Structure.Arrow (x, wt)), k warr)))
    | Constr (Structure.Prod tys) ->
        let (t1, t2) = assume_pair tys in
        let wprod = Var.fresh "wprod" in
        bind t1 (fun w1 -> bind t2 (fun w2 -> Exist (wprod, Some (Structure.Prod [w1; w2]), k wprod)))

  (** This function generates a typing constraint from an untyped term:
      [has_type env t w] generates a constraint [C] which contains [w] as
      a free inference variable, such that [C] has a solution if and only
      if [t] is well-typed in [env], and in that case [w] is the type of [t].

      For example, if [t] is the term [lambda x. x], then [has_type env t w]
      generates a constraint equivalent to [∃?v. ?w = (?v -> ?v)].

      Precondition: when calling [has_type env t], [env] must map each
      term variable that is free in [t] to an inference variable.
  *)
  let rec has_type (env : env) (t : Untyped.term) (w : variable) : (STLC.term, err) t =
    match t with
    | Untyped.Var x ->
        let vtyv = Env.find x env in
        let+ () = eq w vtyv
        in STLC.Var x
    | Untyped.App (t, u) ->
        let wu = Var.fresh "wu" in
        let wt = Var.fresh "wt" in
        Exist (wu, None, 
        Exist (wt, Some (Arrow (wu,w)), 
          let+ f = has_type env t wt
          and+ a = has_type env u wu
          in STLC.App (f,a)
        ))
    | Untyped.Abs (x, t) ->
        let xty = Var.fresh (Untyped.Var.name x) in
        let wt = Var.fresh "wt" in
        let warr = Var.fresh "warr" in
        Exist(xty, None,
        Exist(wt, None,
        Exist(warr, Some (Arrow (xty, wt)),
          let+ () = eq w warr
          and+ argty = decode xty
          and+ body = has_type (Env.add x xty env) t wt
          in STLC.Abs (x, argty, body)
        )))
    | Untyped.Let (x, t, u) ->
        let xty = Var.fresh (Untyped.Var.name x) in
        Exist(xty, None,
          let+ binding = has_type env t xty
          and+ xt = decode xty
          and+ body = has_type (Env.add x xty env) u w
          in STLC.Let (x, xt, binding, body)
        )
    | Untyped.Annot (t, ty) ->
        let k tyv =
          let+ body = has_type env t tyv
          and+ () = eq tyv w
          in STLC.Annot (body, ty)
        in bind ty k
    | Untyped.Tuple ts ->
        let (t1, t2) = assume_pair ts in
        let wprod = Var.fresh "wprod" in
        let w1 = Var.fresh "w1" in
        let w2 = Var.fresh "w2" in
        Exist(w1, None,
        Exist(w2, None,
        Exist(wprod, Some (Prod [w1; w2]),
        let+ e1 = has_type env t1 w1
        and+ e2 = has_type env t2 w2
        and+ () = eq w wprod
        in STLC.Tuple [e1; e2]
        )))
    | Untyped.LetTuple (xs, t, u) ->
        let (x1, x2) = assume_pair xs in
        let wprod = Var.fresh "wt" in
        let w1 = Var.fresh (Untyped.Var.name x1) in
        let w2 = Var.fresh (Untyped.Var.name x2) in
        Exist(w1, None,
        Exist(w2, None,
        Exist(wprod, Some (Prod [w1; w2]),
        let+ binding = has_type env t wprod
        and+ t1 = decode w1
        and+ t2 = decode w2
        and+ body = has_type (Env.add x1 w1 (Env.add x2 w2 env)) u w
        in STLC.LetTuple ([(x1, t1); (x2, t2)], binding, body)
        )))
    | Do p ->
        Constraint.Do (T.map (fun t -> has_type env t w) p)
end
