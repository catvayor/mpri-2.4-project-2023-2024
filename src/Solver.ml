(*
   As explained in the README.md ("Abstracting over an effect"),
   this module as well as other modules is parametrized over
   an arbitrary effect [T : Functor].
*)

module Make (T : Utils.Functor) = struct
  module Constraint = Constraint.Make(T)
  module SatConstraint = SatConstraint.Make(T)
  module ConstraintSimplifier = ConstraintSimplifier.Make(T)
  module ConstraintPrinter = ConstraintPrinter.Make(T)

  type env = Unif.Env.t
  type log = PPrint.document list

  let make_logger c0 =
    let logs = Queue.create () in
    let c0_erased = SatConstraint.erase c0 in
    let add_to_log env =
      let doc =
        c0_erased
        |> ConstraintSimplifier.simplify env
        |> ConstraintPrinter.print_sat_constraint
      in
      Queue.add doc logs
    in
    let get_log () =
      logs |> Queue.to_seq |> List.of_seq
    in
    add_to_log, get_log

  (** See [../README.md] ("High-level description") or [Solver.mli]
      for a description of normal constraints and
      our expectations regarding the [eval] function. *)
  type ('a, 'e) normal_constraint =
    | NRet of 'a Constraint.on_sol
    | NErr of 'e
    | NDo of ('a, 'e) Constraint.t T.t

  let eval (type a e) ~log (env : env) (c0 : (a, e) Constraint.t)
    : log * env * (a, e) normal_constraint
  =
    let add_to_log, get_log =
      if log then make_logger c0
      else ignore, (fun _ -> [])
    in
    let rec reduce : type a e . env -> (a, e) Constraint.t -> env * (a, e) normal_constraint = fun env -> function
      | Ret sol -> (env, NRet sol)
      | Err e -> (env, NErr e)
      | Map (m, f) -> begin
          match reduce env m with
          | (env, NRet sol) -> (env, NRet (fun s -> f (sol s)))
          | (env, NErr e) -> (env, NErr e)
          | (env, NDo m) -> (env, NDo (T.map (fun x -> Constraint.Map(x,f)) m))
        end
      | MapErr (m, f) -> begin
          match reduce env m with
          | (env, NRet sol) -> (env, NRet sol)
          | (env, NErr e) -> (env, NErr (f e))
          | (env, NDo m) -> (env, NDo (T.map (fun x -> Constraint.MapErr(x,f)) m))
        end
      | Conj (m1, m2) -> begin
          let (env, rm1) = reduce env m1 in
          let (env, rm2) = reduce env m2 in
          match rm1, rm2 with
          | NErr e, _ | _, NErr e -> (env, NErr e)
          | NRet sol1, NRet sol2 -> (env, NRet (fun e -> (sol1 e, sol2 e)))
          | NDo m1, NRet sol2 -> (env, NDo (T.map (fun x -> Constraint.Conj(x, Constraint.Ret sol2)) m1))
          | NRet sol1, NDo m2 -> (env, NDo (T.map (fun x -> Constraint.Conj(Constraint.Ret sol1, x)) m2))
          | NDo m1, NDo m2 -> (env, NDo (T.map (fun x -> Constraint.Conj(x, Do m2)) m1))
        end
      | Eq (v1, v2) -> begin
          match Unif.unify env v1 v2 with
          | Ok env -> add_to_log env; (env, NRet (fun _ -> ()))
          | Error err -> begin
              match err with
              | Unif.Clash ((c1, c2)) ->
                  (env, NErr (Constraint.Clash ((Decode.decode env c1, Decode.decode env c2))))
              | Unif.Cycle (v) -> (env, NErr (Constraint.Cycle v))
          end
        end
      | Exist (v, str, m) ->
          let env = Unif.Env.add v str env in
          add_to_log env;
          reduce env m
      | Decode v -> (env, NRet (fun f -> f v))
      | Do p -> (env, NDo p)
    in
    add_to_log env;
    let (env', ret) = reduce env c0 in
    (get_log (), env', ret)

end
