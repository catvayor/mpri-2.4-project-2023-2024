module Make(M : Utils.MonadPlus) = struct
  module Untyped = Untyped.Make(M)
  module Constraint = Constraint.Make(M)
  module Infer = Infer.Make(M)
  module Solver = Solver.Make(M)

  (* just in case... *)
  module TeVarSet = Untyped.Var.Set
  module TyVarSet = STLC.TyVar.Set

let (let*) c f = M.map f c

let untyped : Untyped.term =
  let rec gen env : Untyped.term =
    let open Untyped in
    Do (M.delay @@ fun () ->
      M.sum [
        M.return (App(gen env, gen env));
        (let* v = M.one_of (Array.of_list env) in Var v);
        (let v = Untyped.Var.fresh "x" in M.return (Abs (v, gen (v::env))));
        (let v = Untyped.Var.fresh "x" in M.return (Let (v, gen env, gen (v::env))));
        M.return (Tuple [gen env; gen env]);
        (let x,y = Untyped.Var.fresh "x", Untyped.Var.fresh "y" in
          M.return (LetTuple ([x; y], gen env, gen (x::y::env))));
      ]
    )
  in gen []

let constraint_ : (STLC.term, Infer.err) Constraint.t =
  let w = Constraint.Var.fresh "final_type" in
  Constraint.(Exist (w, None,
    Infer.has_type
      Untyped.Var.Map.empty
      untyped
      w))

let typed ~depth =
  let rec gen depth env cons =
    match (depth, Solver.eval ~log:false env cons) with
    | (0, _) -> M.fail
    | (_, (_, _, NErr _)) -> M.fail
    | (1, (_, env, NRet sol)) -> M.return (sol (Decode.decode env))
    | (_, (_, env, NDo cons)) -> M.bind cons (gen (depth - 1) env)
    | _ -> M.fail
  in
  gen depth Unif.Env.empty constraint_

end
