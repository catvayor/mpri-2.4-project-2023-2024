type 'a t = unit -> 'a option

let map (f : 'a -> 'b) (s : 'a t) : 'b t =
  fun () -> Option.map f (s ())

let return (x : 'a) : 'a t =
  fun () -> Some x

let bind (sa : 'a t) (f : 'a -> 'b t) : 'b t =
  fun () -> Option.bind (sa ()) (fun s -> f s ())

let delay (f : unit -> 'a t) : 'a t =
  fun () -> f () ()

let fail : 'a t =
  fun () -> None

let sum (li : 'a t list) : 'a t =
  match li with
  | [] -> fail
  | _ -> fun () -> List.nth li (Random.int (List.length li)) ()

let one_of (vs : 'a array) : 'a t =
  if Array.length vs = 0 then
    fail
  else
    fun () -> Some vs.(Random.int (Array.length vs))

let run (s : 'a t) : 'a Seq.t =
  let rec runner () =
    match s () with
    | Some v -> v
    | None -> runner ()
  in Seq.forever runner
